from setuptools import setup, find_packages

setup(name='xickle',
      version='1.1',
      py_modules=['xickle'],
      description='XML-persistence, in the most python-ic way',
      author='Hasan Diwan',
      author_email='hasan.diwan@gmail.com',
      install_requires=['xmltodict==0.9.0'],
      license='PSF'
)
